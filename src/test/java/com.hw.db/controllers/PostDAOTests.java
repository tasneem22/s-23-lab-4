package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

class PostDAOTests {
    JdbcTemplate mockJdbc;
    PostDAO postDAO;
    PostDAO.PostMapper POST_MAPPER;
    
    Post post;
    String setPostSQL = "SELECT * FROM \"posts\" WHERE id=? LIMIT 1;";
    static final Integer postID = 1;

    @BeforeEach
    void setup() {
        this.mockJdbc = Mockito.mock(JdbcTemplate.class);
        this.postDAO = new PostDAO(this.mockJdbc);
        post = new Post("author", new Timestamp(1L), "forum", "message", 0, 0, false);
        Mockito.when(mockJdbc.queryForObject(Mockito.eq(setPostSQL), Mockito.any(PostDAO.PostMapper.class),
                        Mockito.eq(postID))).thenReturn(post);
    }

    @Test
    @DisplayName("set post test#1")
    void SetPostTest1() {
            Post newPost = new Post("author2", new Timestamp(2L), "forum", "message2", 0, 0, false);
            PostDAO.setPost(postID, newPost);
            String q = "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;";
            Mockito.verify(mockJdbc).update(Mockito.eq(q), Mockito.anyString(), Mockito.anyString(),
            Mockito.any(Timestamp.class), Mockito.anyInt());
    }
    @Test
    @DisplayName("set post test#2")
    void setPostTest2() {
        Post newPost = new Post("author", new Timestamp(2L), "forum", "message2", 0, 0, false);
        PostDAO.setPost(postID, newPost);
        String q = "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;";
        Mockito.verify(mockJdbc).update(Mockito.eq(q), Mockito.anyString(), Mockito.any(Timestamp.class),
        Mockito.anyInt());
    }
    @Test
    @DisplayName("set post test#3")
    void setPostTest3() {
        Post newPost = new Post("author", new Timestamp(1L), "forum", "message2", 0, 0, false);
        PostDAO.setPost(postID, newPost);
        String q = "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;";
        Mockito.verify(mockJdbc).update(Mockito.eq(q), Mockito.anyString(), Mockito.anyInt());
    }
    @Test
    @DisplayName("set post test#4")
    void setPostTest4() {
        Post newPost = new Post("author", new Timestamp(1L), "forum", "message2", 0, 0, false);
        PostDAO.setPost(postID, newPost);
        String q = "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;";
        Mockito.verify(mockJdbc).update(Mockito.eq(q), Mockito.anyString(), Mockito.anyInt());
    }

}