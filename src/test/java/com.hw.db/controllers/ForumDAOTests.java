package com.hw.db.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

class ForumDAOTests {
    JdbcTemplate mockJdbc;
    ForumDAO forum;
    String since = "12.02.2019";
    String slug = "slug";
    int limit = 10;
    UserDAO.UserMapper USER_MAPPER;
    String userListSQL = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext";

    @BeforeEach
    void setupThreadList() {
        mockJdbc = mock(JdbcTemplate.class);
        forum = new ForumDAO(mockJdbc);
        USER_MAPPER = new UserDAO.UserMapper();
    }

  @Test
  @DisplayName("get list of users test#1")
  void UserListTest1() {
      userListSQL += " AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;";

      ForumDAO.UserList(slug,limit, since, true);
      verify(mockJdbc).query(Mockito.eq(userListSQL), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
  }
  @Test
  @DisplayName("get list of users test#2")
  void UserListTest2() {
      userListSQL += " ORDER BY nickname;";
      ForumDAO.UserList(slug,null, null, false);
      verify(mockJdbc).query(Mockito.eq(userListSQL), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
  }
  @Test
  @DisplayName("get list of users test#3")
  void UserListTest3() {
      userListSQL += " AND  nickname > (?)::citext ORDER BY nickname;";
      ForumDAO.UserList(slug,null, since, false);
      verify(mockJdbc).query(Mockito.eq(userListSQL), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
  }
}