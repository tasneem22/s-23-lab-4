package com.hw.db.controllers;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import java.util.List;
class UserDAOTests {
      JdbcTemplate mockJdbc;
      UserDAO user;
      String changeListSQL = "UPDATE \"users\" SET ";

      @BeforeEach
      void setupThreadList() {
            mockJdbc = mock(JdbcTemplate.class);
            user = new UserDAO(mockJdbc);
      }

      @Test
      @DisplayName("change user data test#1")
      void updateUserTest1() {
            User testUser = new User("nickname", null, null, null);
            UserDAO.Change(testUser);
            verifyNoInteractions(mockJdbc);
      }
      @Test
      @DisplayName("change user data test#2")
      void updateUserTest2() {
            changeListSQL += " email=?  WHERE nickname=?::CITEXT;" ;
            User testUser = new User("nickname", "email", null, null);
            UserDAO.Change(testUser);
            verify(mockJdbc).update(Mockito.eq(changeListSQL), Mockito.anyString(),  Mockito.anyString());
      }
      @Test
      @DisplayName("change user data test#3")
      void updateUserTest3() {
            changeListSQL += " fullname=?  WHERE nickname=?::CITEXT;" ;
            User testUser = new User("nickname", null, "fullName", null);
            UserDAO.Change(testUser);
            verify(mockJdbc).update(Mockito.eq(changeListSQL),  Mockito.anyString(), Mockito.anyString());
      }
      @Test
      @DisplayName("change user data test#4")
      void updateUserTest4() {
            changeListSQL += " about=?  WHERE nickname=?::CITEXT;" ;
            User testUser = new User("nickname", null, null, "about");
            UserDAO.Change(testUser);
            verify(mockJdbc).update(Mockito.eq(changeListSQL),  Mockito.anyString(), Mockito.anyString()); 
     }
      @Test
      @DisplayName("change user data test#5")
      void updateUserTest5() {
            changeListSQL += " email=? , fullname=?  WHERE nickname=?::CITEXT;" ;
            User testUser = new User("nickname", "email", "fullName", null);
            UserDAO.Change(testUser);
            verify(mockJdbc).update(Mockito.eq(changeListSQL), Mockito.anyString(), Mockito.anyString(),
            Mockito.anyString());
      }
      @Test
      @DisplayName("change user data test#6")
      void updateUserTest6() {
            changeListSQL += " email=? , about=?  WHERE nickname=?::CITEXT;" ;
            User testUser = new User("nickname", "email", null, "about");
            UserDAO.Change(testUser);
            verify(mockJdbc).update(Mockito.eq(changeListSQL), Mockito.anyString(), Mockito.anyString(),
            Mockito.anyString());
      }
      @Test
      @DisplayName("change user data test#7")
      void updateUserTest7() {
            changeListSQL += " fullname=? , about=?  WHERE nickname=?::CITEXT;" ;
            User testUser = new User("nickname", null, "fullName", "about");
            UserDAO.Change(testUser);
            verify(mockJdbc).update(Mockito.eq(changeListSQL), Mockito.anyString(), Mockito.anyString(),
            Mockito.anyString());
      }
      @Test
      @DisplayName("change user data test#8")
      void updateUserTest8() {
            changeListSQL += " email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;" ;
            User testUser = new User("nickname", "email", "fullName", "about");
            UserDAO.Change(testUser);
            verify(mockJdbc).update(Mockito.eq(changeListSQL), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(),
            Mockito.anyString());
      }
}